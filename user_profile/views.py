from django.shortcuts import render
from music_index_generator.models import Song

from user_profile.models import Playlist, PlaylistSong
import json
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse

mimetype = 'application/json'


@require_http_methods(["POST"])
@login_required(login_url="/login/")
def create_playlist(request):
    user = None
    if request.user is not None and request.user.is_authenticated():
        user = request.user
    else:
        structure = {'status': False, 'error': 'need authentication'}
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
    print request.body
    in_json = json.loads(request.body)
    playlist = Playlist()
    playlist.user = user
    playlist.name = in_json['name']
    playlist.save()
    structure = {'status': True,
                 'playlist': {'name': playlist.name, 'id': playlist.id, 'date_created': playlist.creation_date,
                              'songs': []}}
    result_json = json.dumps(structure, cls=DjangoJSONEncoder)
    response = HttpResponse(result_json, mimetype)
    return response


@login_required(login_url="/login/")
def all_user_playlists(request):
    try:
        user = request.user
        user_playlists = Playlist.objects.filter(user=user, is_active=True)
        structure = {'status': True}
        res = []
        for playlist in user_playlists:
            out = {'name': playlist.name, 'date_created': playlist.creation_date, 'id': playlist.id,
                   'songs': [x.song.id for x in playlist.playlistsong_set.filter(is_active=True)]}
            res.append(out)
        structure['result'] = res
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
    except Exception as e:
        structure = {'status': False, 'error': str(e)}
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response


@login_required(login_url="/login/")
def add_song_to_playlist(request):
    try:
        structure = {'status': True}
        res = {}
        in_json = json.loads(request.body)
        playlist = Playlist.objects.get(id=in_json['playlist_id'])
        if playlist is not None and playlist.user.id == request.user.id:
            song = Song.objects.get(id=in_json['song_id'])
            if song is not None:
                pls = PlaylistSong()
                pls.song = song
                pls.playlist = playlist
                pls.save()
            else:
                raise Exception("invalid song id")
        else:
            raise Exception("invalid song id")
        structure['result'] = res
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
    except Exception as e:
        structure = {'status': False, 'error': str(e)}
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response


@login_required(login_url="/login/")
def remove_song_from_playlist(request):
    try:
        structure = {'status': True}
        res = {}
        in_json = json.loads(request.body)
        pls = PlaylistSong.objects.get(song_id=in_json['song_id'], playlist_id=in_json['playlist_id'])
        if pls is not None and pls.playlist.user.id == request.user.id:
            pls.is_active = False
            pls.save()
        else:
            raise Exception("invalid playlist")

        structure['result'] = res
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
    except Exception as e:
        structure = {'status': False, 'error': str(e)}
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response


from django.core.cache import caches


@login_required(login_url="/login/")
def add_to_user_q(request):
    try:
        user = request.user
        user_q = caches[user.id]
        if user_q is None:
            user_q = []
        in_json = json.loads(request.body)
        user_q.append(in_json['song_id'])
        caches[user.id] = user_q

        structure = {'status': True}
        res = user_q
        structure['result'] = res
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
    except Exception as e:
        structure = {'status': False, 'error': str(e)}
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response


@login_required(login_url="/login/")
def get_user_q(request):
    try:
        user = request.user
        user_q = caches[user.id]
        if user_q is None:
            user_q = []

        structure = {'status': True}
        res = user_q
        structure['result'] = res
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
    except Exception as e:
        structure = {'status': False, 'error': str(e)}
        result_json = json.dumps(structure, cls=DjangoJSONEncoder)
        response = HttpResponse(result_json, mimetype)
        return response
