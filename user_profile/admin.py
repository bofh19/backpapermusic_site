from django.contrib import admin

# Register your models here.
from user_profile.models import *

class PlaylistSongAdminInline(admin.StackedInline):
	extra = 0
	model = PlaylistSong

class PlaylistAdmin(admin.ModelAdmin):
	list_display = ('user', 'name', 'is_active')
	inlines = [PlaylistSongAdminInline]

admin.site.register(Playlist, PlaylistAdmin)

admin.site.register(PlaylistSong)
