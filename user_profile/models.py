from django.db import models

# Create your models here.
from music_index_generator.models import *
from django.contrib.auth.models import User


class Playlist(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=512)
    creation_date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return str(self.user) + ' - ' + self.name


class PlaylistSong(models.Model):
    playlist = models.ForeignKey(Playlist)

    song = models.ForeignKey(Song)

    display_order = models.DecimalField(max_digits=10, decimal_places=10, default=0.0)

    is_active = models.BooleanField(default=True)

    def __unicode__(self):
        return str(self.playlist) + " - " + str(self.song)
