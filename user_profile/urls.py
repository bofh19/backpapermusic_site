__author__ = 'w4rlock'

from django.conf.urls import patterns, url

urlpatterns = patterns('user_profile.views',
                       url(r'^playlist/create/$', 'create_playlist', name='create_playlist'),
                       url(r'^playlist/all/$', 'all_user_playlists', name='all_user_playlists'),
                       url(r'^playlist/add/$', 'add_song_to_playlist', name='add_song_to_playlist'),
                       url(r'^playlist/remove/$', 'remove_song_from_playlist', name='remove_song_from_playlist'),

url(r'^q/get/$', 'get_user_q', name='get_user_q'),
url(r'^q/add/$', 'add_to_user_q', name='add_to_user_q'),

                       )
