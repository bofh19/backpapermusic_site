HEADERS = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
'Accept-Encoding':'gzip,deflate,sdch',
'Accept-Language':'en-US,en;q=0.8,te;q=0.6',
'Cache-Control':'max-age=0',
'Connection':'keep-alive',
'Cookie':'__cfduid=d6f033d19ebdb4524db2ce28dec6b251d1399837691589',
'Host':'doregama.info',
'Referer':'http://doregama.info/',
'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36'
}

HEADERS_ROCKDAWAY = {
'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
'Accept-Encoding':'gzip,deflate,sdch',
'Accept-Language':'en-US,en;q=0.8,te;q=0.6',
'Cache-Control':'max-age=0',
'Connection':'keep-alive',
'Host':'rockdaway.com',
'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36'
}


import urllib2
from bs4 import BeautifulSoup as bs
import zlib
main_url = 'http://doregama.info'

def get_soup_from_url(url):
	request = urllib2.Request(url,headers=HEADERS)
	f=(urllib2.urlopen(request,timeout=10))
	decompressed_data=zlib.decompress(f.read(), 16+zlib.MAX_WBITS)
	return bs(decompressed_data)
def get_cat_links():
	links = []
	url = 'http://doregama.info/telugu'
	s=get_soup_from_url(url)
	spans = s.find_all('span',attrs={'style': 'color: #00ff00;'})

	for span in spans:
		try:
			link = main_url+span.parent['href']
			print link
			links.append(link)
		except Exception, e:
			print e
	return links


def get_mlinks_for_each_link(link):
	m_links = []
	url = link
	s=get_soup_from_url(url)
	divs = s.find_all('div',attrs={'class':'azindex'})
	for div in divs:
		anchors = div.find_all('a')
		for anchor in anchors:
			try:
				m_links.append(anchor['href'])
			except Exception, e:
				print e
	return m_links

def get_final_download_link_rocked_away(r_link):
	request = urllib2.Request(r_link,headers=HEADERS_ROCKDAWAY)
	f=(urllib2.urlopen(request))
	decompressed_data=zlib.decompress(f.read(), 16+zlib.MAX_WBITS)
	s = bs(decompressed_data)
	for anchor in s.find_all('a'):
		try:
			href = anchor['href']
			if 'zip' in href:
				return href
		except Exception, e:
			print 'error while trying to get href on random anchor',e

# returns object
#
class MovieLink:
	images_links = []
	download_link_320 = ''
	download_link_128 = ''
	data = ''

	def set_download_link_128(self,rlink):
		print 'getting 128 link',rlink
		self.download_link_128 = get_final_download_link_rocked_away(rlink)

	def set_download_link_320(self,rlink):
		print 'getting 320 link',rlink
		self.download_link_320 = get_final_download_link_rocked_away(rlink)

def get_download_links_for_m_link(m_link):
	ml = MovieLink()
	s = get_soup_from_url(m_link)

	main_div = s.find('div',attrs={'class':'post-content'})

	imgs = []

	for img in main_div.find_all('img'):
		try:
			imgs.append(img['src'])
		except Exception, e:
			print 'error while getting image link',e

	ml.images_links = imgs

	ps = main_div.find_all('p')
	next_one_320 = False
	next_one_128 = False
	for p in ps:
		if(next_one_320):
			try:
				xlink = p.find('a')['href']
				if 'rockdaway' in xlink:
					ml.set_download_link_320(p.find('a')['href'])
				else:
					print '320 no rockdaway'
				next_one_320 = False
			except Exception, e:
				next_one_320 = False
				print 'error while getting 320',e
		if(next_one_128):
			try:
				xlink = p.find('a')['href']
				if 'rockdaway' in xlink:
					ml.set_download_link_128(p.find('a')['href'])
				else:
					print '128 no rockdaway'
				next_one_128 = False
			except Exception, e:
				next_one_128 = False
				print 'error while getting 128',e
		if '320' in p.get_text():
			next_one_320 = True
		if '128' in p.get_text():
			next_one_128 = True
	return ml

import os
import gc
def execute_download():
	cat_links = get_cat_links()
	for count,cat_link in enumerate(cat_links):
		if count is 0 or count is 1:
			continue
		print 'executing category link ',cat_link
		m_links = get_mlinks_for_each_link(cat_link)
		for m_link in m_links:
			print count,m_link
			print '------------------------------------------------------------'
			print 'executing m_link',m_link
			a = get_download_links_for_m_link(m_link)
			print 'fetched download links'
			print '////////////////////////////////////////////////////////////'
			print a.images_links
			print a.download_link_320
			print a.download_link_128
			print '////////////////////////////////////////////////////////////'
			print '320 wget ',a.download_link_320
			if a.download_link_320 is not None:
				command = 'wget -N '+a.download_link_320
				os.system(command)
			print 'ignoring 128 for now'
			# print '128 wget ',a.download_link_128
			# if a.download_link_128 is not None:
			# 	command = 'wget -N '+a.download_link_128
			# 	os.system(command)
			print '------------------------------------------------------------',gc.collect()

from os import walk

def is_number(s):
	try:
		float(s)
		return True
	except Exception, e:
		return False
# does not work as intended don't use this
def create_folders():
	pathx = '/home/praveenr/temp/music'
	f = []
	for dirpath,dirname,filenames in walk(pathx):
		f.extend(filenames)
		break
	dirs = []
	for fx in f:
		z=False
		a=''
		for x in reversed(fx.split('_')):
			if z:
				a += x+" "
			if is_number(x):
				z=True
		print a
		dirs.append(pathx+'/'+a)
	for d in dirs:
		print d
		if not os.path.exists(d):
			os.makedirs(d)

# does not work as intended don't use this
def fix_folders():
	pathx = '/home/praveenr/temp/music'
	pathx2 = '/home/praveenr/temp/music2'
	if not os.path.exists(pathx2):
		os.makedirs(pathx2)
	dirs = []
	for dirpath,dirname,filenames in walk(pathx):
		dirs.extend(dirname)
		break
	for d in dirs:
		dirfiles = []
		for dirpath,dirname,filenames in walk(pathx+'/'+d):
			dirfiles.extend(filenames)
			break
		if(len(dirfiles) >= 1):
			if(len(dirfiles) >= 2):
				for dirfile in dirfiles:
					if '320' in dirfile:
						command = 'unzip '+pathx+'/'+d+'/'+dirfile+' -d '+pathx+'/'+d
						print 'COMMAND: ',command
						os.system(command)
						command = 'rm -rf '+pathx+'/'+d+'/'+dirfile
						print 'COMMAND: ',command
						os.system(command)
						break
				newdir = []
				leftfile = []
				for dirpath,dirname,filenames in walk(pathx+'/'+d+'/'):
					newdir.extend(dirname)
					leftfile.extend(filenames)
				newdir = newdir[0]
				leftfile = leftfile[0]
				command = 'unzip '+pathx+'/'+d+'/'+leftfile+' -d '+pathx+'/'+d
				new_create_dir = leftfile.replace('.zip','')
				new_create_dir = new_create_dir.replace('.ZIP','')
				print 'COMMAND: ',command
				os.system(command)
				command = 'rm -rf '+pathx+'/'+d+'/'+leftfile
				print 'COMMAND: ',command
				os.system(command)
				command = 'mv '+pathx+'/'+d+'/'+new_create_dir+' '+pathx+'/'+d+'/'+newdir
				print 'COMMAND: ',command
				os.system(command)
		elif(len(dirfiles) == 1):
			command = 'unzip '+pathx+'/'+d+'/'+dirfile[0]+' -d '+pathx+'/'+d
			print 'COMMAND: ',command
			os.system(command)
			command = 'rm -rf '++pathx+'/'+d+'/'+dirfile[0]
			print 'COMMAND: ',command
			os.system(command)

		command = 'mv '+pathx+'/'+d+' '+pathx2+'/'+d
		print 'COMMAND: ',command
		os.system(command)

import pipes
def fix_folders2():
	pathx = '/home/praveenr/temp/music2/'
	pathx2 = '/home/praveenr/temp/music3/'
	dirs = []
	for dirpath,dirname,filenames in os.walk(pathx):
		dirs.extend(dirname)
		break
	print dirs

	for d in dirs:
		d_dirs = []
		cpath = pathx+d
		print cpath
		for dirpath,dirname,filenames in os.walk(cpath):
			d_dirs.extend(dirname)
			break
		print d_dirs
		if(len(d_dirs) >= 2):
			d320 = None
			dother = None
			for d_dir in d_dirs:
				if '320' in d_dir:
					d320 = d_dir
				else:
					dother = d_dir
			if dother is not None and d320 is not None:
				command = 'mv \''+pathx+d+'/'+dother+'\' \''+pathx+d+'/'+d320+'\''
				print 'COMMAND: ',command
				os.system(command)

		command = 'mv \''+pathx+d+'/\' '+pathx2
		print 'COMMAND: ',command
		os.system(command)

def fix_folders3():
	path = '/home/praveenr/temp/music4/'
	dirs = []
	for dirpath,dirname,filenames in os.walk(path):
		dirs.extend(dirname)
		break

	for d in dirs:
		ddirs = []
		for dirpath,dirname,filenames in os.walk(path+d):
			ddirs.extend(dirname)
			break
		if(len(ddirs)>=1):
			print 'exists renaming now',ddirs[0]
			command = 'mv \''+path+d+'/'+ddirs[0]+'\' \''+path+d+'/'+ddirs[0]+'_128kbps\''
			print 'COMMAND: ',command
			os.system(command)

	








