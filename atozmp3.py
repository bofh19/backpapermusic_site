HEADERS = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
'Accept-Encoding':'gzip,deflate,sdch',
'Accept-Language':'en-US,en;q=0.8,te;q=0.6',
'Cache-Control':'max-age=0',
'Connection':'keep-alive',
'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36'
}

ESKIMO_HEADERS = {
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.78.2 (KHTML, like Gecko) Version/7.0.6 Safari/537.78.2'
}

import urllib2
from bs4 import BeautifulSoup as bs
import zlib
import tempfile
import zipfile
import uuid
from azureStorage import list_blobs, upload_files_to_folder as azuploadfiles, upload_file_to_folder as azuploadfile


MASTER_CONTAINER_NAME = "backpapermusic-parsed"
AZURE_ENABLED = True


def get_soup_from_url(url):
    print 'getting data for',url
    request = urllib2.Request(url,headers=HEADERS)
    f=(urllib2.urlopen(request,timeout=10))
    decompressed_data=zlib.decompress(f.read(), 16+zlib.MAX_WBITS)
    return bs(decompressed_data)
def get_cat_links():
    links = []
    url = 'https://www.atozmp3.co/'
    s=get_soup_from_url(url)
    element = s.find('aside', attrs={'id': 'custom_html-5'})
    table = element.find('table')

    for ea in table.find_all('a'):
        try:
            link = ea['href']
            if link == '#':
                print 'skipping', link, ea
            else:
                print 'adding',link,ea
            links.append(link)
        except Exception, e:
            print e
    return links

def get_next_page_in_cat_url(soup):
    main_divs = soup.find_all('div',attrs={'class':'pagenavi clear'})
    if len(main_divs) <= 0:
        return None
    main_divs = main_divs[0]
    for a in main_divs.find_all():
        if 'Next' in a.get_text():
            return a['href']
    return None

def get_mlinks_for_page(link):
    print 'getting mlinks data for',link
    mlinks = []
    s = get_soup_from_url(link)
    for each_h2 in s.find_all('h2', attrs={'class': 'entry-title'}):
        mlinks.append(each_h2.find('a')['href'])
    return mlinks, s

def get_mlinks_for_each_link(link):
    print 'getting mlinks for link ', link
    m_links, s = get_mlinks_for_page(link)
    next_link = s.find('a', attrs={'class':'next page-numbers'})
    if next_link is not None:
        m_links += get_mlinks_for_each_link(next_link['href'])
    return m_links

def get_final_download_link_eskimo_away(r_link):
    request = urllib2.Request(r_link,headers=ESKIMO_HEADERS)
    f=(urllib2.urlopen(request))
    s = bs(f.read())
    allas = s.find_all('a')
    for a in allas:
        if 'zip' in a['href']:
            return a['href']

# returns object
#
class MovieLink:
    image_link = None
    download_link_320 = None
    download_link_128 = None
    data = ''
    src = None
    heading = None

    def __init__(self, src):
        self.src = src

    def set_download_link_128(self,rlink):
        print 'getting 128 link',rlink
        self.download_link_128 = get_final_download_link_eskimo_away(rlink)

    def set_download_link_320(self,rlink):
        print 'getting 320 link',rlink
        self.download_link_320 = get_final_download_link_eskimo_away(rlink)

def get_download_links_for_m_link(m_link):
    ml = MovieLink(m_link)
    s = get_soup_from_url(m_link)
    allas = s.find_all('a')
    is320Done = False
    is128Done = False
    for a in allas:
        if 'DirectLink' in a.get_text():
            if '320' in  a.parent.parent.get_text() and is320Done is not True:
                ml.set_download_link_320(a['href'])
                is320Done = True
            if '128' in  a.parent.parent.get_text() and is128Done is not True:
                ml.set_download_link_128(a['href'])
                is128Done = True
    ml.image_link = get_download_image_if_possible(s)
    ml.heading = fix_heading(s.find('article').find('h1').text)
    return ml

def fix_heading(heading):
    heading = heading.replace('/','-')
    remove_strs = ['Telugu Mp3', 'Telugu Album','Songs','Download','MP3','Mp3','Free']
    for each_str in remove_strs:
        heading = heading.replace(each_str,'')
    return heading

def get_download_image_if_possible(s):
    try:
        article = s.find('article')
        img = article.find('img')
        return img['src']
    except:
        return None

def fetch_download_links_and_download(m_link, condition=None):
    a = get_download_links_for_m_link(m_link)
    print 'fetched download links'
    print '////////////////////////////////////////////////////////////'
    print a.image_link
    print a.download_link_320
    print a.download_link_128
    print a.heading
    print '////////////////////////////////////////////////////////////'
    print '320 wget ', a.download_link_320

    if condition is not None:
        if not condition(a.heading):
            print 'skipping condition to download failed'
            return None, None

    downloaded = False
    current_temp_folder = os.path.join(tempfile.gettempdir(), str(uuid.uuid4()))
    os.makedirs(current_temp_folder)
    if a.download_link_320 is not None:
        try:
            command = 'wget -N "{0}" -P {1}'.format(a.download_link_320, current_temp_folder)
            os.system(command)
            downloaded = True
        except Exception, e:
            pass
    elif a.download_link_128 is not None:
        try:
            command = 'wget -N "{0}" -P {1}'.format(a.download_link_128, current_temp_folder)
            os.system(command)
            downloaded = True
        except Exception, e:
            pass
    else:
        print 'missing both 128 and 320'
    if downloaded and a.image_link is not None and len(a.image_link) > 0:
        try:
            command = 'wget -N "{0}" -P {1}'.format(a.image_link, current_temp_folder)
            os.system(command)
        except:
            pass
    # print '128 wget ',a.download_link_128
    # if a.download_link_128 is not None:
    # 	command = 'wget -N '+a.download_link_128
    # 	os.system(command)
    if downloaded:
        print 'downloaded to folder ', current_temp_folder
    return current_temp_folder if downloaded else None, a.heading

def fetch_download_links_and_download_and_upload(m_link):
    def should_download(name):
        blobs = list_blobs(container_name=MASTER_CONTAINER_NAME, prefix=name)
        for each_blob in blobs:
            if each_blob is not None \
                    and each_blob.name is not None \
                    and each_blob.name.index(name) == 0:
                return False
        return True
    if AZURE_ENABLED:
        folder_path, heading = fetch_download_links_and_download(m_link, condition=should_download)
        if folder_path is not None:

            for each_object in os.listdir(folder_path):
                abs_path = os.path.join(folder_path, each_object)
                if os.path.isfile(abs_path):
                    filename, file_extension = os.path.splitext(abs_path)
                    if file_extension.lower() == '.jpg':
                        azuploadfile(MASTER_CONTAINER_NAME, abs_path, 'thumb.jpg', heading)

                if zipfile.is_zipfile(abs_path):
                    zip_extract_to = os.path.join(folder_path, 'tmpextract')
                    zfile = zipfile.ZipFile(abs_path)
                    zfile.extractall(zip_extract_to)
                    upload_folder_path = get_mp3_actual_dir(zip_extract_to)
                    try:
                        azuploadfiles(MASTER_CONTAINER_NAME, upload_folder_path, heading)
                    except Exception, ex:
                        print 'unable to upload to azure'
                        print ex
            remove_folder(folder_path)
    else:
        folder_path, heading = fetch_download_links_and_download(m_link)
    return True

def get_mp3_actual_dir(dir_path):
    for dirname, dirnames, filenames in os.walk(dir_path):
        for filename in filenames:
            basename, extension = os.path.splitext(os.path.join(dirname, filename))
            if extension.lower() == '.mp3':
                return os.path.abspath(dirname)

def remove_folder(dir_path):
    for root, dirs, files in os.walk(dir_path, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))

import os
import gc
import multiprocessing
MAX_WORKER_POOL = 10

def execute_download():
    cat_links = get_cat_links()
    pool = multiprocessing.Pool(MAX_WORKER_POOL)
    workers = []
    skip = [u'https://www.atozmp3.co/tag/A',
 u'https://www.atozmp3.co/tag/B',
 u'https://www.atozmp3.co/tag/C',
 u'https://www.atozmp3.co/tag/D',
 u'https://www.atozmp3.co/tag/E',
 u'https://www.atozmp3.co/tag/F',
 u'https://www.atozmp3.co/tag/G',
 u'https://www.atozmp3.co/tag/H',
 u'https://www.atozmp3.co/tag/I',
 u'https://www.atozmp3.co/tag/j',
 u'https://www.atozmp3.co/tag/K',
 u'https://www.atozmp3.co/tag/L',
 u'https://www.atozmp3.co/tag/M',
 u'https://www.atozmp3.co/tag/N',
 u'https://www.atozmp3.co/tag/O',
 u'https://www.atozmp3.co/tag/P',
 u'https://www.atozmp3.co/tag/Q',
 u'https://www.atozmp3.co/tag/R',
 u'https://www.atozmp3.co/tag/S',
 u'https://www.atozmp3.co/tag/T',
 u'https://www.atozmp3.co/tag/U',
 u'https://www.atozmp3.co/tag/V']
    for count,cat_link in enumerate(cat_links):
        print 'executing category link ',cat_link
        if cat_link in skip:
            continue
        m_links = get_mlinks_for_each_link(cat_link)
        for m_link in m_links:
            while len(workers) >= MAX_WORKER_POOL:
                #print 'workers len',len(workers), 'pausing and cleaning up'
                done_idx = []
                for idx, each_worker in enumerate(workers):
                    if each_worker.ready():
                        done_idx.append(idx)
                workers = [worker for idx, worker in enumerate(workers) if idx not in done_idx]

            print count,m_link
            print '------------------------------------------------------------'
            print 'executing m_link',m_link

            workers.append(pool.apply_async(fetch_download_links_and_download_and_upload, (m_link,)))
            #fetch_download_links_and_download_and_upload(m_link)

            print '------------------------------------------------------------',gc.collect()

    # wait till all workers are done
    while len(workers) >= MAX_WORKER_POOL:
        # print 'workers len',len(workers), 'pausing and cleaning up'
        done_idx = []
        for idx, each_worker in enumerate(workers):
            if each_worker.ready():
                done_idx.append(idx)
        workers = [worker for idx, worker in enumerate(workers) if idx not in done_idx]