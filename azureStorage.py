from azure.storage.blob import BlockBlobService, PublicAccess
from azure.common import AzureConflictHttpError
import os

ACCOUNT_NAME = ''
ACCOUNT_KEY = ''

def upload_files_to_folder(container_name, disk_directory_path, blob_folder_name):
    block_blob_service = BlockBlobService(account_name=ACCOUNT_NAME, account_key=ACCOUNT_KEY)

    try:
        block_blob_service.create_container(container_name, fail_on_exist=True)
    except Exception, ex:
        print type(ex), type(ex) == AzureConflictHttpError
        if not type(ex) == AzureConflictHttpError:
            raise ex


    for each_object in os.listdir(disk_directory_path):
        abs_path = os.path.abspath(os.path.join(disk_directory_path, each_object))
        if os.path.isfile(abs_path):
            blob_name = blob_folder_name + '/' + each_object
            policy = block_blob_service.create_blob_from_path(container_name, blob_name, abs_path)


def upload_file_to_folder(container_name, disk_abs_file_path, file_name, blob_folder_name):
    block_blob_service = BlockBlobService(account_name=ACCOUNT_NAME, account_key=ACCOUNT_KEY)

    try:
        block_blob_service.create_container(container_name, fail_on_exist=True)
    except Exception, ex:
        print type(ex), type(ex) == AzureConflictHttpError
        if not type(ex) == AzureConflictHttpError:
            raise ex

    if os.path.isfile(disk_abs_file_path):
        blob_name = blob_folder_name + '/' + file_name
        policy = block_blob_service.create_blob_from_path(container_name, blob_name, disk_abs_file_path)



def list_blobs(container_name, prefix):
    block_blob_service = BlockBlobService(account_name=ACCOUNT_NAME, account_key=ACCOUNT_KEY)
    try:
        return block_blob_service.list_blobs(container_name, prefix=prefix)
    except:
        return []