from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib import admin
from django.contrib.auth import views as admin_views
from web_ui.views import LoginForm
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'backpapermusic_site.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/',include('music_index_generator.urls')),
    url(r'ui/', include('web_ui.urls')),
    url(r'user/', include('user_profile.urls')),

    url('^', include('django.contrib.auth.urls')),

)+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
