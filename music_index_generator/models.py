from django.db import models

# Create your models here.
from backpapermusic_site.settings import MEDIA_ROOT, THUMB_IMAGE
import eyed3
import os


def log_error_eyed3(msg, path):
    pass


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (THUMB_IMAGE, ext)
    return os.path.join('music', instance.dirname.dirname, filename)


class Dir(models.Model):
    dirname = models.CharField(max_length=255, unique=True)
    is_new = models.BooleanField(default=False)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return self.dirname

    def save(self, **kwargs):
        generated_counters = GeneratedCounters()
        generated_counters.songs_added = 1
        generated_counters.save()
        super(Dir, self).save()


class Song(models.Model):
    album = models.CharField(max_length=255, blank=True, null=True)
    publisher = models.CharField(max_length=255, blank=True, null=True)
    artist = models.CharField(max_length=255, blank=True, null=True)
    title = models.CharField(max_length=255, blank=True, null=True)
    genre = models.CharField(max_length=255, blank=True, null=True)
    track_num = models.IntegerField(blank=True, null=True)

    thumb_file = models.ImageField(upload_to=get_file_path, blank=True, null=True)
    song_file = models.FileField(upload_to=get_file_path)

    # readonly fields
    dirname = models.ForeignKey(Dir)
    bit_rate = models.CharField(max_length=255, blank=True, null=True)
    size_bytes = models.CharField(max_length=1000, blank=True, null=True)
    time_secs = models.IntegerField(blank=True, null=True)

    # this will be either DEFAULT,128,8
    bit_rate_dir = models.CharField(max_length=255, default='DEFAULT')

    java_hash_key = models.IntegerField(unique=True)
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        name = ''
        if (self.title is not None):
            name += self.title
        if self.album is not None:
            name += " - " + self.album
        if self.artist is not None:
            name += " - " + self.artist
        name += self.song_file.name
        return name

    def save(self, **kwargs):
        if (self.id is None):
            print 'id is none must be new'
            super(Song, self).save()
            return;
        try:
            f = eyed3.load(os.path.join(MEDIA_ROOT, self.song_file.name))
            f.tag.album = self.album
            f.tag.publisher = self.publisher
            f.tag.title = self.title
            f.tag.genre = self.genre
            f.tag.track_num = self.track_num
            f.tag.artist = self.artist
            f.tag.save()
        except Exception, e:
            print 'failed in model ', str(e)
            log_error_eyed3('failed to edit settings from model ' + str(e), self.song_file.name)
        super(Song, self).save()

        # saves thumbnail data for all songs if uploaded for a particular song in that dirname
        try:
            if self.thumb_file is not None:
                imagedata = open(os.path.join(MEDIA_ROOT, self.thumb_file.name), "rb").read()
                for each_song in Song.objects.filter(dirname=self.dirname):
                    f = eyed3.load(os.path.join(MEDIA_ROOT, each_song.song_file.name))
                    f.tag.images.set(3, imagedata, "image/jpeg", each_song.dirname.dirname)
                    f.tag.save()
        except Exception, e:
            print 'failed save in thumbnail model ', str(e)
            log_error_eye3('failed save in thumbnail model ' + str(e), self.song_file.name)

    def selflink(self):
        if self.id:
            return "<a href='/admin/music_index_generator/song/%s' target='_blank'>Edit</a>" % str(self.id)
        else:
            return "Not present"

    selflink.allow_tags = True


class GeneratedCounters(models.Model):
    generated_date = models.DateTimeField(verbose_name=("Generated date"), auto_now_add=True, blank=True, null=True)
    songs_added = models.IntegerField()

    def __unicode__(self):
        return str(self.generated_date) + '/' + str(self.songs_added)


class ErrorLogger(models.Model):
    creation_date = models.DateTimeField(verbose_name=("Creation date"), auto_now_add=True, blank=True, null=True)
    error_msg = models.TextField()
    error_path = models.TextField()

    def __unicode__(self):
        return str(self.error_path) + " / " + str(self.error_msg)
