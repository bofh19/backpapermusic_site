from all_funcs import *

from django.db.models import Count
from django.core.serializers.json import DjangoJSONEncoder
from django.views.decorators.clickjacking import xframe_options_exempt


@xframe_options_exempt
def music_list_db_json(request):
    path = MEDIA_ROOT + '/' + MUSIC_FOLDER + '/'
    url_path = MEDIA_URL + MUSIC_FOLDER + '/'
    structure = []
    dirs = Song.objects.values('dirname').annotate(dcount=Count('dirname'))
    structure = []
    for each_dir in dirs:
        dir_songs = Song.objects.filter(dirname=each_dir['dirname'])
        this_dir_songs = []
        for each_song in dir_songs:
            mp3file = {}
            full_name = each_song.song_file.name
            full_name = full_name.split('/')[-1]
            mp3file['id'] = each_song.id
            mp3file['name'] = full_name
            mp3file['track_url'] = each_song.song_file.url
            mp3file['album'] = each_song.album
            mp3file['artist'] = each_song.artist
            mp3file['genre'] = each_song.genre
            mp3file['publisher'] = each_song.publisher
            mp3file['title'] = each_song.title
            mp3file['track_num'] = each_song.track_num
            mp3file['bit_rate'] = each_song.bit_rate
            mp3file['size_bytes'] = each_song.size_bytes
            mp3file['time_secs'] = each_song.time_secs
            mp3file['dirname'] = each_song.dirname.dirname
            mp3file['hash_key'] = each_song.java_hash_key
            mp3file['is_new'] = each_song.dirname.is_new
            mp3file['creation_date'] = each_song.creation_date
            mp3file['bit_rate_dir'] = each_song.bit_rate_dir
            try:
                mp3file['thumb_url'] = each_song.thumb_file.url
            except Exception, e:
                mp3file['thumb_url'] = None

            this_dir_songs.append(mp3file)

        if (len(this_dir_songs) > 0):
            structure.append(this_dir_songs)

    result_json = json.dumps(structure, cls=DjangoJSONEncoder)
    response = HttpResponse(result_json, mimetype)
    # response['Access-Control-Allow-Origin'] = '*'
    # response['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    # response['Access-Control-Max-Age'] = 1000
    # response['Access-Control-Allow-Headers'] = '*'
    return response


def modified_date(request):
    generated_counters = GeneratedCounters.objects.all().order_by('-generated_date')[0]
    structure = {}
    structure['generated_date'] = generated_counters.generated_date
    structure['songs_added'] = generated_counters.songs_added
    result_json = json.dumps(structure, cls=DjangoJSONEncoder)
    return HttpResponse(result_json, mimetype)
