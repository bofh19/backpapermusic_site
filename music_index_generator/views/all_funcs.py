from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.

# Create your views here.\
from django.http import HttpResponse
from urlparse import urlparse
from django.shortcuts import render_to_response
from django.template import RequestContext
from backpapermusic_site.settings import MEDIA_ROOT
from backpapermusic_site.settings import MUSIC_ROOT, MUSIC_FOLDER, HMAC_PRIVATE_KEY

import random
import os
import json

mimetype = 'application/json'
import eyed3

from backpapermusic_site.settings import MEDIA_URL


def get_jpg_at_this(path):
    for each_file in sorted(os.listdir(path)):
        filename, fileext = os.path.splitext(os.path.join(path, each_file))
        if fileext == '.jpg' or fileext == '.png' or fileext == '.jpeg' or fileext == '.JPG' or fileext == '.PNG' or fileext == '.GIF' or fileext == '.gif':
            return each_file
    return None


import unicodedata


# does nothing for now
def get_normalize_string(val):
    # return unicodedata.normalize('NFKD', val).encode('ascii','ignore')
    return val


from music_index_generator.models import *

import time, datetime
