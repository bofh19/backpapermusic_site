from all_funcs import *

from error_log import *
from generate_thumbs import *


def does_this_dir_has_any_audio(path):
    for each_file in sorted(os.listdir(path)):
        filename, fileext = os.path.splitext(os.path.join(path, each_file))
        if fileext == '.mp3' or fileext == '.MP3' or fileext == '.wav' or fileext == '.WAV' or fileext == '.ogg':
            return True
    return False


def java_string_hashcode(s):
    h = 0
    for c in s:
        h = (31 * h + ord(c)) & 0xFFFFFFFF
    return ((h + 0x80000000) & 0xFFFFFFFF) - 0x80000000


def test_clear_all():
    for song in Song.objects.all():
        song.delete()
    for each_dir in Dir.objects.all():
        each_dir.delete()
    for gccounters in GeneratedCounters.objects.all():
        gccounters.delete()
    for el in ErrorLogger.objects.all():
        el.delete()


# TODO : fix it to work on all not just 128kbps
def music_list_generate_all2():
    test_clear_all()
    print 'generating thumbs'
    generate_thumbs()
    print 'generating thumbs ends sarting normal'
    songs_generated_count = 0
    path = MEDIA_ROOT + '/' + MUSIC_FOLDER + '/'
    url_path = MEDIA_URL + MUSIC_FOLDER + '/'

    if (GeneratedCounters.objects.all().count() > 0):
        last_generated_time = GeneratedCounters.objects.all().order_by('-generated_date')[0]
        last_generated_time = last_generated_time.generated_date
        last_generated_time = last_generated_time.replace(tzinfo=None)
    else:
        last_generated_time = datetime.datetime(1, 1, 1, 0, 0)

    for name in sorted(os.listdir(path)):
        if os.path.isdir(os.path.join(path, name)):
            jpeg_at_this_path = get_jpg_at_this(os.path.join(path, name))
            if jpeg_at_this_path is not None:
                thumb_url = url_path + name + '/' + jpeg_at_this_path
                thumb_path = MUSIC_FOLDER + '/' + name + '/' + jpeg_at_this_path
            else:
                thumb_url = None
                thumb_path = None

            new_dir = Dir.objects.get_or_create(dirname=name)[0]
            print "last modified: %s" % time.ctime(os.path.getmtime(os.path.join(path, name)))
            print "created: %s" % time.ctime(os.path.getctime(os.path.join(path, name)))
            dir_modified_date = datetime.datetime.fromtimestamp(os.path.getmtime(os.path.join(path, name)))
            if (dir_modified_date >= last_generated_time):
                print '////////////////////////////this directory has to be added'
                for each_file in sorted(os.listdir(os.path.join(path, name))):
                    if os.path.isdir(os.path.join(path, name, each_file)):
                        print "************************************"
                        print "===================================="
                        print os.path.join(path, name, each_file)

                        for each_file_level2 in sorted(os.listdir(os.path.join(path, name, each_file))):
                            filename, fileext = os.path.splitext(os.path.join(path, name, each_file, each_file_level2))
                            if fileext == '.mp3' or fileext == '.MP3' or fileext == '.wav' or fileext == '.WAV' or fileext == '.ogg':
                                song_file_path = MUSIC_FOLDER + '/' + name + '/' + each_file + '/' + each_file_level2
                                song_file_url = url_path + name + "/" + each_file + '/' + each_file_level2
                                songs_generated_count = save_song(new_dir, thumb_url, thumb_path, each_file_level2,
                                                                  song_file_path, song_file_url, name,
                                                                  songs_generated_count, True, each_file)
                        print "===================================="
                        print "************************************"
                    if os.path.isfile(os.path.join(path, name, each_file)):
                        filename, fileext = os.path.splitext(os.path.join(path, name, each_file))
                        if fileext == '.mp3' or fileext == '.MP3' or fileext == '.wav' or fileext == '.WAV' or fileext == '.ogg':
                            # new_dir has the current directory name from here
                            # thumb_url has the current director thumbnail file url
                            # thumb_path has the current directory file path
                            # each_file has the current operating mp3
                            # song_file_path has the path of file on disk
                            # song_file_url has the path of file on url
                            song_file_path = MUSIC_FOLDER + '/' + name + '/' + each_file
                            song_file_url = url_path + name + "/" + each_file
                            songs_generated_count = save_song(new_dir, thumb_url, thumb_path, each_file, song_file_path,
                                                              song_file_url, name, songs_generated_count, False, None)
            else:
                print '////////////////////////////this directory cannot be added'
    generated_counters = GeneratedCounters()
    generated_counters.songs_added = songs_generated_count
    generated_counters.save()
    print songs_generated_count


def save_song(new_dir, thumb_url, thumb_path, each_file, song_file_path, song_file_url, name, songs_generated_count,
              is128, dir2):
    s = Song()
    s.song_file.name = song_file_path
    actual_song_jpeg_path = generate_thumb(os.path.join(path, name, each_file))
    print '------------actual_song_jpeg_path',actual_song_jpeg_path
    if actual_song_jpeg_path is not None:
        thumb_file_name = os.path.basename(actual_song_jpeg_path)
        actual_song_thumb_file_path = MUSIC_FOLDER + '/' + name + '/' + thumb_file_name
        s.thumb_file.name = actual_song_thumb_file_path
    else:
        s.thumb_file.name = thumb_path
    s.dirname = new_dir
    s.java_hash_key = java_string_hashcode(new_dir.dirname + "/" + each_file)
    if is128:
        s.bit_rate_dir = '128'
        if dir2 is not None:
            s.java_hash_key = java_string_hashcode(new_dir.dirname + "/" + dir2 + "/" + each_file)
    try:
        print 'first save calling'
        s.save()
        songs_generated_count += 1
        print 'first save called'
    except Exception, e:
        print 'initial save failed ', str(e)

    eyed3_load_failed = False
    try:
        f = eyed3.load(os.path.join(path, name, each_file))
    except Exception, e:
        print e, "failed on", os.path.join(path, name, each_file)
        log_error_eyed3("failed to load eyed3 " + str(e), os.path.join(path, name, each_file))
        eyed3_load_failed = True

    if not eyed3_load_failed:
        try:
            s.album = get_normalize_string(f.tag.album)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.artist = get_normalize_string(f.tag.artist)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.genre = get_normalize_string(f.tag.genre.name)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.publisher = get_normalize_string(f.tag.publisher)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.title = get_normalize_string(f.tag.title)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.track_num = get_normalize_string(f.tag.track_num[0])
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.bit_rate = get_normalize_string(f.info.bit_rate_str)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.size_bytes = get_normalize_string(f.info.size_bytes)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)
        try:
            s.time_secs = get_normalize_string(f.info.time_secs)
        except Exception, e:
            print e, "failed on", os.path.join(path, name, each_file)

        try:
            print 'second save calling'
            s.save()
            print 'second save called'
        except Exception, e:
            log_error_eyed3("total failure on save " + str(e), s.song_file.name)
            print "////////////////////////////failed to save RETRY FAIELD", str(e)



    return songs_generated_count
