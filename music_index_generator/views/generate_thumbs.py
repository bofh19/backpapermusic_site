from backpapermusic_site.settings import MEDIA_ROOT, MUSIC_FOLDER, THUMB_IMAGE

path = MEDIA_ROOT + '/' + MUSIC_FOLDER
import eyed3
import os

from all_funcs import *


def generate_thumbs():
    for name in os.listdir(path):
        if os.path.isdir(os.path.join(path, name)):
            print "check for jpg at path ", os.path.join(path, name)
            if get_jpg_at_this(os.path.join(path, name)) is None:
                for each_file in os.listdir(os.path.join(path, name)):
                    if os.path.isfile(os.path.join(path, name, each_file)):
                        filename, fileext = os.path.splitext(os.path.join(path, name, each_file))
                        if fileext == '.mp3' or fileext == '.MP3':
                            try:
                                af = eyed3.load(os.path.join(path, name, each_file))
                                x = open(os.path.join(path, name, THUMB_IMAGE + '.jpg'), 'w+')
                                x.write(af.tag.images[0].image_data)
                                x.close()
                                print 'saved on ', os.path.join(path, name, THUMB_IMAGE + '.jpg')
                                break
                            except Exception, e:
                                print 'failed on ', os.path.join(path, name, each_file)
            else:
                print "jpg already exist"


import uuid

from saavn_temp import helper_get_file_data


def generate_thumb(mp3_path):
    try:
        af = eyed3.load(mp3_path)
        dirpath = os.path.dirname(mp3_path)
        try:
            if af.tag.copyright_url is not None and len(af.tag.copyright_url) > 0:
                url = af.tag.copyright_url
                url = url.replace("-150x150", "-500x500")
                data = helper_get_file_data(url)
                if len(data) > 0:
                    x = open(os.path.join(dirpath, str(uuid.uuid4()) + ".jpg"), 'w+')
                    x.write(data)
                    x.close()
                    print '-------GOT FILE FROM URL-----',url,x.name
                    return x.name
                else:
                    raise Exception("")
            else:
                raise Exception("")
        except Exception, ex:
            x = open(os.path.join(dirpath, str(uuid.uuid4()) + ".jpg"), 'w+')
            x.write(af.tag.images[0].image_data)
            x.close()
            return x.name
    except Exception, e:
        print 'unable to get image data ', e
        return None
