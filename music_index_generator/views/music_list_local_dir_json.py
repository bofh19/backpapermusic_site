from all_funcs import *

def music_list_local_dir_json(request):
	path=MEDIA_ROOT+'/'+MUSIC_FOLDER+'/'
	url_path = MEDIA_URL+MUSIC_FOLDER+'/'
	structure = []
	for name in sorted(os.listdir(path)):
		if os.path.isdir(os.path.join(path,name)):
			jpeg_at_this_path = get_jpg_at_this(os.path.join(path,name))
			if jpeg_at_this_path is not None:
				thumb_url = url_path+name+'/'+ jpeg_at_this_path
				thumb_path = MUSIC_FOLDER+'/'+name+'/'+ jpeg_at_this_path
			else:
				thumb_url = None
				thumb_path = None 
			this_dir = []
			for each_file in sorted(os.listdir(os.path.join(path,name))):
				if os.path.isfile(os.path.join(path,name,each_file)):
					filename,fileext=os.path.splitext(os.path.join(path,name,each_file))
					if fileext=='.mp3' or fileext=='.MP3' or fileext=='.wav' or fileext=='.WAV' or fileext=='.ogg':
						song_file_path = MUSIC_FOLDER+'/'+name+'/'+each_file
						mp3file = {}
						mp3file['name'] = each_file
						mp3file['track_url'] = url_path+name+"/"+each_file
						mp3file['album'] = None
						mp3file['artist'] = None
						mp3file['genre'] = None
						mp3file['publisher'] = None
						mp3file['title'] = None
						mp3file['track_num'] = None
						mp3file['bit_rate'] = None
						mp3file['size_bytes'] = None
						mp3file['time_secs'] = None
						mp3file['dirname'] = name
						mp3file['thumb_url'] = thumb_url
						try:
							f = eyed3.load(os.path.join(path,name,each_file))
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							print "f.tag.album: ",f.tag.album
							mp3file['album'] = get_normalize_string(f.tag.album)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['artist'] = get_normalize_string(f.tag.artist)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['genre'] = get_normalize_string(f.tag.genre.name)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['publisher'] = get_normalize_string(f.tag.publisher)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['title'] = get_normalize_string(f.tag.title)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['track_num'] = get_normalize_string(f.tag.track_num[0])
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['bit_rate'] = get_normalize_string(f.info.bit_rate_str)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['size_bytes'] = get_normalize_string(f.info.size_bytes)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)
						try:
							mp3file['time_secs'] = get_normalize_string(f.info.time_secs)
						except Exception, e:
							print e,"failed on",os.path.join(path,name,each_file)

						this_dir.append(mp3file)
			if(len(this_dir) > 0):
				structure.append(this_dir)
	result_json=json.dumps(structure)
	return HttpResponse(result_json,mimetype)