
from music_index_generator.models import ErrorLogger

def log_error(msg,path):
	el = ErrorLogger()
	el.error_msg = msg
	el.error_path = path
	el.save()

def log_error_eyed3(msg,path):
	log_error("EYED3 "+msg,path)