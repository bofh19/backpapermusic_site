__author__ = 'w4rlock'

from django.conf.urls import patterns, url

urlpatterns = patterns('music_index_generator.views',
	url(r'^music_list_dir/$', 'music_list_local_dir_json', name='music_list_local_dir_json'),
	url(r'^music_list/$', 'music_list_db_json', name='music_list_db_json'),
	url(r'^modified_date/$', 'modified_date', name='modified_date'),
)