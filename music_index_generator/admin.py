from django.contrib import admin

# Register your models here.

from music_index_generator.models import *

class SongModelAdmin(admin.ModelAdmin):
	model = Song
	list_display = (
		'id',
		'song_name',
		'title',
		'album',
		'track_num',
		'artist',
		'genre',
		'publisher',		
		'dirname',
		'bit_rate_dir',
		'bit_rate',
		'size_bytes',
		'time_secs',
		)
	list_editable = ('album',
		'publisher',
		'artist',
		'title',
		'track_num',
		'genre',
		'title',)
	list_filter = ('dirname',)

	fieldsets = (
		(None ,{'fields':(
					'id',
					'song_name',
					'album',
					'publisher',
					'artist',
					'title',
					'genre',
					'track_num',
					'dirname',
					'bit_rate',
					'size_bytes',
					'time_secs',
					'image_thumb',
					'song_audio',
					'thumb_file',
					'song_file',
					'bit_rate_dir'
					)})
		,)

	def song_name(self,obj):
		return obj.song_file.name
	def image_thumb(self,obj):
		return u'<img src="%s" />' % ( obj.thumb_file.url )
	def song_audio(self,obj):
		return u"""<audio controls="" autobuffer="" height="5px" style="left: 200px" class="audio-controls" id="audio_id" src="%s">
            <source src="" type="audio/mpeg">
            Your browser does not support the audio element.
          </audio>""" % (obj.song_file.url)
	def get_readonly_fields(self, request, obj=None):
		return ('id','dirname','bit_rate','size_bytes','time_secs','song_name','image_thumb','song_audio','song_file')
	image_thumb.short_description = 'Album Thumbnail'
	image_thumb.allow_tags = True
	song_audio.short_description = "Song file"
	song_audio.allow_tags = True

admin.site.register(Song,SongModelAdmin)


class SongModelAdminInline(admin.TabularInline):
	extra = 0
	model = Song
	list_filter = ('dirname',)
	fieldsets = (
		(None ,{'fields':(
					'id',
					'selflink',
					'song_name',
					'album',
					'publisher',
					'artist',
					'title',
					'genre',
					'track_num',
					'dirname',
					'bit_rate',
					'size_bytes',
					'time_secs',
					'image_thumb',
					'song_audio',
					'thumb_file',
					'song_file'
					)})
		,)

	def song_name(self,obj):
		return obj.song_file.name
	def image_thumb(self,obj):
		return u'<img src="%s" height=75/>' % ( obj.thumb_file.url )
	def song_audio(self,obj):
		return obj.song_file.name 
	def get_readonly_fields(self, request, obj=None):
		return ('id','selflink','dirname','bit_rate','size_bytes','time_secs','song_name','image_thumb','song_audio','song_file')
	image_thumb.short_description = 'Album Thumbnail'
	image_thumb.allow_tags = True
	song_audio.short_description = "Song file"
	song_audio.allow_tags = True


from django import forms
class DirForm(forms.ModelForm):
	x = forms.CharField(widget=forms.Textarea)
	class Meta:
		model = Dir
		fields = '__all__'

class DirModelAdmin(admin.ModelAdmin):
	model = DirForm
	inlines = [SongModelAdminInline]

admin.site.register(Dir,DirModelAdmin)
admin.site.register(GeneratedCounters)
admin.site.register(ErrorLogger)


