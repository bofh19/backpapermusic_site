__author__ = 'saikrishnak'
import json
import requests
import urllib
from PIL import Image
import os

import eyed3

def helper_get_file_data(image_url):
    headers_i = {
        'GET' : image_url,
        'Host': 'c.saavncdn.com',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'en-US,en;q=0.8,te;q=0.6'
    }
    r2i = requests.get(image_url, data=None, headers=headers_i)
    img_file = image_url.split('/')[-1]
    with open(img_file, 'wb') as f:
        for chunk in r2i.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    image_data = open(img_file, "rb").read()
    os.remove(img_file)
    return image_data


try:
    urllib_encode = urllib.urlencode
except:
    urllib_encode = urllib.parse.urlencode

def save_song(key_code, image_url, js, prepend_file=None):
    file_name = js['title']
    file_name = file_name

    try:
        album_name = js["album"]
    except:
        album_name = ""

    if prepend_file is not None:
        save_file = "{0} {1} [{2}].mp3".format(prepend_file, file_name, album_name)
    else:
        save_file = "{0} [{1}].mp3".format(file_name, album_name)

    save_file = save_file.replace("/","")

    if(os.path.isfile(save_file) ):
        print("already exists file for ", save_file)
        return

    url = "https://www.saavn.com/api.php"

    headers = {
        'Host' : 'www.saavn.com',
        'POST':  '/api.php HTTP/1.1',
        'Connection': 'keep-alive',
        'Content-Length': '139',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Origin': 'http://www.saavn.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Referer': 'http://www.saavn.com/play/featured/telugu/Weekly+Top+Songs',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.8,te;q=0.6',
        'Cookie': 'B=2610d9721529f58477221c986ba33175; CT=MTAyMDI3MDcwOA==; __gads=ID=9c1f74a09df426c8:T=1502927420:S=ALNI_MZ3_TIcVrR7ljuzrXthOuvqbFXbWQ; geo=73.254.85.208%2CUS%2CWashington%2CBellevue%2C98007; CH=G03%2CA07%2CO00%2CL03; __utmt=1; L=telugu; _fp=409155f4cd2ada03f565c31970c886db; ATC=k%2F8Qge30BBDFA9awtmRbYBRG0tjEszBGN556GCHV7GWyKrkaUnKqBowGprmWc4Cf; jwplayer.volume=75; __utma=257722889.780132431.1502927409.1506563198.1506587022.7; __utmb=257722889.14.10.1506587022; __utmc=257722889; __utmz=257722889.1502927409.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
    }


    dtx = {
        'url' : key_code,
        '__call': 'song.generateAuthToken',
        '_marker': 'false',
        '_format': 'json',
        'bitrate': '128'
    }

    r = requests.post(url, data=urllib_encode(dtx), json=None, headers=headers)

    print(r)
    print(r.text)

    data = json.loads(r.text)
    if 'auth_url' not in data:
        print '---'*5,'FAILED TO LOAD ', save_file
        return
    url2 = data['auth_url']
    print(url2)

    headers2 = {
        'GET' : url2,
        'Host': 'aa.cf.saavncdn.com',
        'Connection': 'keep-alive',
        'Accept-Encoding': 'identity;q=1, *;q=0',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Accept': '*/*',
        'Referer': 'http://www.saavn.com/play/featured/telugu/Weekly+Top+Songs',
        'Accept-Language': 'en-US,en;q=0.8,te;q=0.6',
        'Range': 'bytes=0-'
    }

    r2 = requests.get(url2, params=None, headers=headers2)


    with open(save_file, 'wb') as f:
        for chunk in r2.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)

    aud_file = eyed3.load(save_file)
    image_orig_url = image_url

#    image_url = image_url.replace("-150x150","");
    headers_i = {
        'GET' : image_url,
        'Host': 'c.saavncdn.com',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'Cache-Control': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'en-US,en;q=0.8,te;q=0.6'
    }

    print(image_url)

    r2i = requests.get(image_url, data=None, headers=headers_i)
    if r2i.status_code == 403:
        print("403 trying with original url")
        image_url = image_orig_url
        headers_i = {
            'GET' : image_url,
            'Host': 'c.saavncdn.com',
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8,te;q=0.6'
        }

        print(image_url)

        r2i = requests.get(image_url, data=None, headers=headers_i)

    print("got image")
    img_file = image_url.split('/')[-1]
    with open(img_file, 'wb') as f:
        for chunk in r2i.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    image_data = open(img_file, "rb").read()

    aud_file.initTag()

    aud_file.tag.images.set(3, image_data, "image/jpeg", img_file)

    small_outfile = img_file+'thumbnail'+'.jpg'
    im = Image.open(img_file)
    size = 32,32
    im.thumbnail(size, Image.ANTIALIAS)
    im.save(small_outfile, "JPEG")
    small_image_data = open(small_outfile, "rb").read()
    aud_file.tag.copyright_url = image_orig_url
    try:
        pass
        #aud_file.tag.images.set(1, small_image_data, "image/jpeg", img_file)
    except Exception as e:
        pass

    try:
        aud_file.tag.images.set(4, image_data, "image/jpeg", img_file)
    except Exception as e:
        pass

    try:
        aud_file.tag.images.set(5, image_data, "image/jpeg", img_file)
    except Exception as e:
        pass

    try:
        aud_file.tag.images.set(6, image_data, "image/jpeg", img_file)
    except Exception as e:
        pass

    try:
        aud_file.tag.artist = u"{}".format(js['music'])
    except Exception as e:
        print(e)

    try:
        aud_file.tag.album = u"{}".format(js['album'])
    except Exception as e:
        print(e)

    try:
        aud_file.tag.year = u"{}".format(js['year'])
    except Exception as e:
        print(e)

    try:
        aud_file.tag.title = u"{}".format(js['title'])
    except Exception as e:
        print(e)

    try:
        aud_file.tag.album_artist = u"{}".format(js['music'])
    except Exception as e:
        print(e)

    try:
        aud_file.tag.comments.set(u"{}".format(js['starring']))
    except Exception as e:
        print(e)

    aud_file.tag.save(version=(2,3,0))
    aud_file.tag.save(version=(1,0,0))
    print("saved tags")

    os.remove(img_file)
    os.remove(small_outfile)



#save_song('NMKyboFo/FiuL1+3zAH3oneSxfJmUvl0rOsw21q0G5Y8XkNjgLI/ZQ==')

#save_song('NMKyboFo/FheNOcQeMfQ7GzN+d9emvP7rBi+suMuy2M8XkNjgLI/ZQ==')
from bs4 import BeautifulSoup as bs

def do_main():
    url_mains = ["http://www.saavn.com/s/featured/telugu/Weekly_Top_Songs?qt=a&st=1&t=1490402174587&ct=1964685750","https://www.saavn.com/play/featured/Array/Top_Telugu_Hits_2017/IKWoswgVMtswkg5tVhI3fw__?qt=a&st=1&t=1506589620952&ct=1020245939"]
    #url_main = "http://www.saavn.com/s/featured/telugu/Weekly_Top_Songs?qt=a&st=1&t=1490402174587&ct=1964685750"
    #url_main = "https://www.saavn.com/s/featured/telugu/Telugu_Chartbusters?qt=a&st=1&t=1490402174587&ct=1964685750"
    headers_main  ={
        #'GET' :'/s/featured/telugu/Weekly_Top_Songs?qt=a&st=1&t=1490402174587&ct=1964685750 HTTP/1.1',
        'Host': 'www.saavn.com',
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, sdch',
        'Accept-Language': 'en-US,en;q=0.8,te;q=0.6',
        'Cookie': 'B=2610d9721529f58477221c986ba33175; CT=MTAyMDI3MDcwOA==; __gads=ID=9c1f74a09df426c8:T=1502927420:S=ALNI_MZ3_TIcVrR7ljuzrXthOuvqbFXbWQ; geo=73.254.85.208%2CUS%2CWashington%2CBellevue%2C98007; CH=G03%2CA07%2CO00%2CL03; __utmt=1; L=telugu; _fp=409155f4cd2ada03f565c31970c886db; ATC=k%2F8Qge30BBDFA9awtmRbYBRG0tjEszBGN556GCHV7GWyKrkaUnKqBowGprmWc4Cf; jwplayer.volume=75; __utma=257722889.780132431.1502927409.1506563198.1506587022.7; __utmb=257722889.14.10.1506587022; __utmc=257722889; __utmz=257722889.1502927409.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
    }
    for url_main in url_mains:
        r_main = requests.get(url=url_main, data=None, headers = headers_main)
        d_main = r_main.text
        soup = bs(d_main, "html.parser")

        a = soup.find_all('div', class_='hide song-json')
        js = json.loads(a[0].text)
        #save_song(js['url'] ,js['image_url'], js)
        counter = 0
        for sxs in soup.find_all('div', class_='hide song-json'):
            counter += 1
            sxs_json = json.loads(sxs.text)
            save_song(sxs_json['url'],sxs_json['image_url'], sxs_json, None)

if __name__ == "__main__":
    do_main()
