 var app = angular.module("MPlayer", ['ui.router', 'ngAnimate'])
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', '$controllerProvider', '$provide', '$compileProvider',
            function ($stateProvider, $urlRouterProvider, $locationProvider ,  $httpProvider,   $controllerProvider,   $provide,   $compileProvider) {
                app._controller = app.controller;
                app._service = app.service;
                app._factory = app.factory;
                app._value = app.value;
                app._directive = app.directive;
                app.controller = function (name, constructor) {
                    $controllerProvider.register(name, constructor);
                    return (this);
                };
                app.service = function (name, constructor) {
                    $provide.service(name, constructor);
                    return (this);
                };
                app.factory = function (name, factory) {
                    $provide.factory(name, factory);
                    return (this);
                };
                app.value = function (name, value) {
                    $provide.value(name, value);
                    return (this);
                };
                app.directive = function (name, factory) {
                    $compileProvider.directive(name, factory);
                    return (this);
                };
                $httpProvider.interceptors.push('httpInterceptorService');
                // Configure client-side routing
                $locationProvider.hashPrefix("!").html5Mode(true);
                $urlRouterProvider.otherwise("/");

                $stateProvider
                    .state('Player', {
                        url: '/player',
                        views: {
                            "mainContainer": {
                                templateUrl: "/media/site/player/player.html"
                            }
                        }
                    })
                    .state('Random', {
                        url: '/random',
                        views: {
                            "mainContainer": {
                                templateUrl: "/media/site/random/random.html"
                            }
                        }
                    })
                    .state('Playlists', {
                        url: '/playlists',
                        views: {
                            "mainContainer": {
                                templateUrl: "/media/site/playlist/playlist.html"
                            }
                        }
                    });
            }]);
            
    //animate on angular hide and show
    app.animation('.slide', function () {
        var NG_HIDE_CLASS = 'ng-hide';
        return {
            beforeAddClass: function (element, className, done) {
                if (className === NG_HIDE_CLASS) {
                    element.slideUp(done);
                }
            },
            removeClass: function (element, className, done) {
                if (className === NG_HIDE_CLASS) {
                    element.hide().slideDown(done);
                }
            }
        }
    });

    function registerWithAngular(containerSelector) {
        var container = $(containerSelector);
        var newScope = angular.element(container).scope();
        var compile = angular.element(container).injector().get('$compile');
        compile($(container).contents())(newScope);
    };


angular.module("MPlayer").controller("MainController", ['$rootScope', 'musicService', function ($rootScope, musicService) {
    var self = this;
    self.siteName = 'Music Player';
    self.pageTitle = '';

    // Update the pageTitle property when the state changes
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        if (toParams.action != undefined) {
            self.pageTitle = self.siteName + ' - ' + toState.name + '-' + toParams.action;
        } else {
            self.pageTitle = self.siteName + ' - ' + toState.name;
        }
    });

    this.init = function(){
        musicService.getFullMusicList().then(function (data) {
            window.castPlayer.addVideoThumbs(data);
        });
    }
    
}]);