angular.module('MPlayer').directive('audioPlayer', ['$parse', '$document', '$timeout', 'audioPlayerService', 
    function($parse, $document, $timeout, audioPlayerService){
        setload = function(scope, load_val){
            $timeout(function(){
                scope.loadstart = load_val;
            },100);
        };
        setError = function(scope, error_val, error_msg){
            $timeout(function(){
                scope.error = error_val;
                scope.error_message = error_msg;
            }, 100);
        };
        return{
            restrict: 'E',
            templateUrl: '/media/site/ang.directives/audio_player/template.html',
            controller: ['$scope', '$element', '$attrs', 'playlistService',
            function AudioPlayerController($scope, $element, $attrs, playlistService) {
                
            }],
            link: function link(scope, element, attrs, controller, transcludeFn) {
                scope.loadstart = false;
                scope.error = false;
                scope.state = "";
                tag = element.find('audio')[0];
                var player = {
                    state: function(){
                        return scope.state;
                    },
                    pause: function(){
                        tag.pause();
                    },
                    resume: function(){
                        tag.resume();
                    },
                    play: function(song){
                        tag.pause();
                        tag.src = song.track_url;
                        tag.innerHTML = song.track_url;
                        img_tag = element.find('img')[0];
                        img_tag.src = song.thumb_url;
                    },
                };
                
                scope.currentSong = function(){
                    return audioPlayerService.getSong();
                };

                audioPlayerService.setPlayer(player);

                tag.oncanplaythrough = function(){
                    setload(scope, false);
                };
                tag.onpause = function(){
                    scope.state = "paused";
                    audioPlayerService.__events.paused();
                }
                tag.oncanplay = function(){
                    if(tag.src != undefined && tag.src.length > 0){
                        scope.state = "playing";
                        tag.play();
                        audioPlayerService.__events.playStarted();
                    }
                };
                tag.onloadedmetadata = function(){
                    console.log("loadedmeta");
                };
                tag.onloadstart = function(){
                    setError(scope, false, undefined);
                    if(tag.src != undefined && tag.src.length > 0){
                        setload(scope, true);
                    };
                };
                tag.onstalled = function(){
                    setError(scope, true, "stalled");
                    scope.state = stalled;
                    audioPlayerService.__events.onerror();
                };
                tag.onerror = function(){
                    setError(scope, true,"error");
                    scope.state = "error";
                    audioPlayerService.__events.onerror();
                };
                tag.onended = function(){
                    scope.state = "ended";
                    console.log("ended");
                    audioPlayerService.__events.onended();
                }
            },
        };
    }]);