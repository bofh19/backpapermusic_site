angular.module('MPlayer').directive('playerPlaylist',
    ['$parse', '$document', 'audioPlayerService', 'playlistService',
        function ($parse, $document, audioPlayerService, playlistService) {
            return {
                restrict: 'E',
                scope: {
                    playlistId: "=playlistId"
                },
                templateUrl: '/media/site/ang.directives/playlist/template.html',
                controller: ['$scope', '$element', '$attrs', 'playlistService',
                    function PlaylistViewController($scope, $element, $attrs, playlistService) {
                        $scope.init = function () {
                            playlistService.getPlaylist($scope.playlistId).then(
                                function (playlist) {
                                    $scope.playlist = playlist;
                                    playlistService.onPlaylistUpdated(function(playlist){
                                        //ignore as angular takes care of this also :)
                                    });
                                });
                        };

                        $scope.playSong = function(song){
                            audioPlayerService.playSong(song);
                        };
                    }
                ],
                link: function link(scope, element, attrs, controller, transcludeFn) {
                    
                },
            };
        }]);