angular.module('MPlayer').directive('albumView', ['$parse', '$document', function ($parse, $document) {
    return {
        restrict: 'E',
        scope: {
            album: "=album",
            onClick: "&onClick",
            playSongParent: "&playSong"
        },
        controller: ['$scope', '$element', '$attrs',
            function AlbumViewController($scope, $element, $attrs) {
                $scope.expand = false;
                $scope.albumClick = function (album) {
                    $scope.onClick(album);
                    $($element).find(".modal").modal({
                        keyboard: false
                    })
                };

                $scope.closeSongsList = function () {
                    $scope.album.showSongs = false;
                };

                $scope.playSong = function (song, action) {
                    $scope.playSongParent({ song: song, action: action });
                };
            }],
        link: function link(scope, element, attrs, controller, transcludeFn) {
        },
        templateUrl: '/media/site/ang.directives/album/template.html'
    }
}]);