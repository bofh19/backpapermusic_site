angular.module('MPlayer').directive('playerPlaylistMain',
    ['$parse', '$document', 'audioPlayerService', 'playlistService',
        function ($parse, $document, audioPlayerService, playlistService) {
            return {
                restrict: 'E',
                scope: {
                },
                templateUrl: '/media/site/ang.directives/playlist_main/template.html',
                controller: ['$scope', '$element', '$attrs', 'playlistService',
                    function PlaylistViewController($scope, $element, $attrs, playlistService) {
                        $scope.init = function () {
                            playlistService.getPlaylists().then(
                                function (playlists) {
                                    $scope.playlists = playlists;
                                });
                        };
                        $scope.createPlaylist = function () {
                            if ($scope.name != undefined && $scope.name.length > 0) {
                                playlistService.createPlaylist($scope.name)
                                    .then(function (playlist) {
                                    });
                            }
                        }
                    }
                ],
                link: function link(scope, element, attrs, controller, transcludeFn) {
                    
                },
            };
        }]);