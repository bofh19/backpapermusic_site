angular.module('MPlayer').directive('songView', ['$parse', '$document', '$timeout', 'playlistService', 'audioPlayerService',
    function ($parse, $document, $timeout, playlistService, audioPlayerService) {
        return {
            restrict: 'E',
            scope: {
                song: "=song",
                playSong: "&playSong"
            },
            controller: ['$scope', '$element', '$attrs',
                function SongViewController($scope, $element, $attrs) {
                    $scope.playThisSongNow = function (song) {
                        $scope.playSong({ song: song, action: 'now' });
                    };

                    $scope.playThisSongNext = function (song) {
                        $scope.playSong({ song: song, action: 'next' });
                    };

                    $scope.cancelAdd = function(){
                        $scope.playlists = undefined;
                    };

                    $scope.addToThisPlaylist = function(playlist){
                        $scope.playlists = undefined;
                        playlistService.addSong(playlist.id, $scope.song, 0);
                    };

                    $scope.addToPlaylist = function(song){
                        playlistService.getPlaylists().then(function(playlists){
                            $scope.playlists = playlists;
                        });
                    };

                    $scope.showPlayOrPause = function(){
                        if( $scope.currentPlayingSong != undefined 
                            && $scope.song.id == $scope.currentPlayingSong.id){
                            if($scope.playerState == "playing"){
                                return "pause";
                            }
                        }
                        return "play";
                    };

                    $scope.showPlay = function(){
                        return $scope.showPlayOrPause() == "play";
                    };

                    $scope.showPause = function(){
                        return $scope.showPlayOrPause() == "pause";
                    };

                    $scope.pauseThisSong = function(){
                        audioPlayerService.pause();
                    };
                }],
            link: function link(scope, element, attrs, controller, transcludeFn) {
                scope.showAddPlaylist = true;
                scope.currentPlayingSong = audioPlayerService.getSong();
                scope.playerState = audioPlayerService.getState();
                var observer = {
                    id: scope.song.id,
                    onplay: function(){
                        console.log("onplay");
                        $timeout(function(){
                            scope.currentPlayingSong = audioPlayerService.getSong();
                            scope.playerState = audioPlayerService.getState();
                        }, 100);
                    },
                    onchange: function(newSong, oldSong){
                        $timeout(function(){
                            scope.currentPlayingSong = newSong;
                            scope.playerState = audioPlayerService.getState();
                        }, 100);
                    },
                    onstop: function(){
                        console.log("onstop");
                        $timeout(function(){
                            scope.currentPlayingSong = audioPlayerService.getSong();
                            scope.playerState = audioPlayerService.getState();
                        }, 100);
                    },
                    onpause: function(){
                        console.log("onpause");
                        $timeout(function(){
                            scope.currentPlayingSong = audioPlayerService.getSong();
                            scope.playerState = audioPlayerService.getState();
                        }, 100);
                    },
                    onerror: function(){
                        console.log("onerror");
                        $timeout(function(){
                            scope.currentPlayingSong = audioPlayerService.getSong();
                            scope.playerState = audioPlayerService.getState();
                        }, 100);
                    }
                };
                audioPlayerService.addObserver(observer);
                if (attrs['hidePlaylistAdd'] != undefined && attrs['hidePlaylistAdd'].toLocaleLowerCase() == "true") {
                    scope.showAddPlaylist = false;
                }
                scope.showThumbnail = false;
                if(attrs['showThumbnail'] != undefined && attrs['showThumbnail'].toLocaleLowerCase() == "true"){
                    scope.showThumbnail = true;
                }

                element.on('$destroy', function() {
                  console.log('elem destroy');
                  audioPlayerService.removeObserver(observer);
                });
            },
            templateUrl: '/media/site/ang.directives/song/template.html'
        }
    }]);