angular.module("MPlayer")
    .controller("PlayerRandomController", ['$rootScope', 'musicService', 'audioPlayerService','randomNumberService','audioPlayerService','$timeout',
        function ($rootScope, musicService, audioPlayerService,randomNumberService,audioPlayerService,$timeout) {
            var self = this;
            self.currentSong = undefined
            self.randomId = randomNumberService.getRandomNumber(19999,29999);
            self.fullmode = true;
            self.init = function () {
                self.playRandomSong()
                var observer = {
                    id: self.randomId,
                    onplay: function(){
                        console.log("onplay");
                    },
                    onchange: function(newSong, oldSong){
                        console.log('onchange');
                    },
                    onstop: function(){
                        console.log("onstop");
                    },
                    onpause: function(){
                        console.log("onpause");
                    },
                    onerror: function(){
                        console.log("onerror");
                    },
                    onended: function(){
                        console.log("on ended");
                        self.playRandomSong();
                    }
                };
                audioPlayerService.addObserver(observer);
            };

            self.playRandomSong = function(){
                musicService.getFullMusicList().then(function (data) {
                    $timeout(function(){
                        songs = []
                        for(i=0;i<data.length;i++){
                            songs = songs.concat(data[i])
                        }
                        songRandom = songs[self.randomNumber(1,songs.length)-1]
                        audioPlayerService.playSong(songRandom);
                        self.currentSong = songRandom;
                    });
                });
            }

            self.randomNumber = function(st,end){
                return randomNumberService.getRandomNumber(st,end)
            }
            self.playSong = function(song, action){
                audioPlayerService.playSong(song);
            };
        }]);