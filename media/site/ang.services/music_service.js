window.api_url = "http://backpapermusic.w4rlock.in/api/music_list/"
window.api_url = "/api/music_list/"
angular.module("MPlayer").factory('musicService',
    ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
        var service = {
            musicList: [],
            uniqueAlbums: [],
            _secret_songs_list: [],
            loading_data: false
        };

        service.getFullMusicList = function () {
            if(service.loading_data){
                return $q(function(resolve, reject){
                    setTimeout(function(){
                        service.getFullMusicList().then(function(data){
                            resolve(service.musicList);
                        })
                    }, 200);
                });
            }
            if (service.musicList.length <= 0) {
                service.loading_data = true;
                return $http.get(window.api_url)
                .then(function (response) {
                    service.musicList = [];
                    service.musicList = response.data;
                    service.uniqueAlbums = [];
                    var max = 0;
                    for(i = 0; i < service.musicList.length; i++){
                        for(j = 0; j < service.musicList[i].length; j++){
                            c_id = service.musicList[i][j].id;
                            if( c_id > max ){
                                max = c_id;
                            }
                        }
                    }
                    _secret_songs_list = [];
                    for(i = 0; i < max; i++){
                        service._secret_songs_list.push(undefined);
                    }
                    for (i = 0; i < service.musicList.length; i++) {
                        if (service.musicList[i].length > 0) {
                            var x = {};
                            angular.copy(service.musicList[i][0], x);
                            if(x.thumb_url != undefined){
                                //x.thumb_url = "http://backpapermusic.w4rlock.in" + x.thumb_url;
                            }
                            if(x.album != undefined){
                                x.album = x.album.trim();
                            }else{
                                x.album = "";
                            }
                            if(x.album.length <= 0){
                                x.album = x.dirname;
                            }
                            service.uniqueAlbums.push(x);
                            x.artists = x.artist;
                            if(x.artists == undefined) x.artists = "";
                            else x.artists = x.artists.trim();
                            for (j = 0; j < service.musicList[i].length; j++) {
                                //service.musicList[i][j].thumb_url = "http://backpapermusic.w4rlock.in" + service.musicList[i][j].thumb_url;
                                //service.musicList[i][j].track_url = "http://backpapermusic.w4rlock.in" + service.musicList[i][j].track_url;
                                var current_artist = service.musicList[i][j].artist;
                                if(current_artist != undefined) current_artist = current_artist.trim();
                                else current_artist = "";
                                if(current_artist.length > 0 && x.artists.indexOf(current_artist) < 0){
                                    x.artists = x.artists + " " + current_artist;
                                }

                                service._secret_songs_list[service.musicList[i][j].id] = service.musicList[i][j];
                            }
                            artists = x.artists.split(",");
                            artists = new Set(artists);
                            artists = Array.from(artists);
                            artists = artists.join(", ")
                            x.artists = artists;
                            delete x["artist"]; 
                            delete x["name"];
                            delete x["track_url"];
                            delete x["track_num"];
                            delete x["time_secs"];
                            delete x["size_bytes"];
                        }
                    }
                    service.loading_data = false;
                    return response.data;
                }).catch(function (response) {
                    return $q.reject(response);
                });
            } else {
                return $q.when(service.musicList);
            }
        }

        service.getAlbumSongs = function (album_name) {
            return service.getFullMusicList()
            .then(function (data) {
                for (i = 0; i < data.length; i++) {
                    if (data[i].length > 0) {
                        if (data[i][0]['album'] == album_name) {
                            return data[i];
                        }
                    }
                }
                return [];
            });
        };

        service.getSong = function(song_id){
            return service.getFullMusicList()
                .then(function(data){
                    if(song_id < service._secret_songs_list.length){
                        return service._secret_songs_list[song_id];
                    }
                    return undefined;
                });
        }

        service.getAlbums = function () {
            return service.getFullMusicList()
            .then(function (data) {
                return service.uniqueAlbums;
            });
        }

        service.clearMusicList = function () {
            var deferred = $q.defer();
            service.musicList = [];
            deferred.resolve();
            return deferred.promise;
        };
        return service;
    }]);