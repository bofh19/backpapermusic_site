angular.module('MPlayer').factory('audioPlayerService', 
    ['$rootScope', '$q', function($rootScope, $q){
        var service = {
            audioPlayerData : {
                currentSong: undefined
            },
            observers : [], // observer.onplay, observer.onpause, observer.onstop, observer.onchange, onerror
            player: undefined,
            __events: {},
        };

        service.addObserver = function(observer){
            var deferred = $q.defer();
            if (observer.onplay == undefined
                || observer.onpause == undefined
                || observer.onstop == undefined
                || observer.onchange == undefined
                || observer.onerror == undefined){
                throw "observer doesnt implement all objects";
            }else{
                found = false;
                for(i=0;i < service.observers.length; i++){
                    if(observer.id == service.observers[i].id){
                        found = true;
                        break;
                    }
                }
                if(!found){
                    service.observers.push(observer);
                }
            }
            return deferred.promise;
        };

        service.removeObserver = function(observer){
            var deferred = $q.defer();
            var which = undefined;
            for(i = 0 ; i < service.observers.length; i++){
                if(service.observers[i].id == observer.id){
                    which = i;
                    break;
                }
            }
            if(which != undefined){
                service.observers.splice(which, 1);
            }
            return deferred.promise;
        };

        service.playSong = function(song){
            var oldSong = undefined;
            if(service.audioPlayerData.currentSong != undefined){
                oldSong = {};
                angular.copy(service.audioPlayerData.currentSong, oldSong);
            }
            service.audioPlayerData.currentSong = song;
            service.player.play(service.audioPlayerData.currentSong);
            service.__events.songChanged(oldSong);
        };

        service.getSong = function(){
            return service.audioPlayerData.currentSong;
        };

        service.getState = function() {
            return service.player.state();
        };

        service.pause = function(){
            if(service.player) service.player.pause();
        };

        service.resume = function(){
            if(service.player) service.player.resume();
        };

        service.setPlayer = function(player){
            service.player = player;

        };

        service.__events.songChanged = function(oldSong){
             angular.forEach(service.observers, function(observer){
                observer.onchange(service.audioPlayerData.currentSong, oldSong);
            });
        };

        service.__events.playStarted = function(){
            angular.forEach(service.observers, function(observer){
                oldSong = {};
                observer.onplay();
            });
        };

        service.__events.paused = function(){
            angular.forEach(service.observers, function(observer){
                observer.onpause();
            });
        };

        service.__events.onstop = function(){
            angular.forEach(service.observers, function(observer){
                observer.onstop();
            });
        };

        service.__events.onerror = function(){
            angular.forEach(service.observers, function(observer){
                observer.onerror();
            });
        }

        service.__events.onended = function(){
            angular.forEach(service.observers, function(observer){
                if(observer.onended){
                    observer.onended();
                }
            })
        }

        return service;
    }]);