angular.module("MPlayer").factory('httpInterceptorService', ['$q', '$injector', '$window', '$timeout',
    function ($q, $injector, $window, $timeout) {
        var self = this;

        self.isUnauthorized = function (response) {
            try {
                return response.data.hasOwnProperty("statusCode") && response.data.statusCode === 401;
            } catch (e) { }

            return false;
        };
        self.isForbidden = function (response) {
            try {
                return response.data.hasOwnProperty("statusCode") && response.data.statusCode === 403;
            } catch (e) { }

            return false;
        };
        self.responseHasError = function (response) {
            try {
                return response.data.hasOwnProperty("success") && response.data.success === false;
            } catch (e) { }

            return false;
        };

        self.blockUI = function () {
            console.log("block ui");
        };
        self.unblockUI = function () {
            console.log("un block ui");
        };

        self.handleHttpError = function () {
            console.log("handle error");
        };


        self.handleForbidden = function () {
            console.log("handle 401");
        };

        self.handleUnauthorized = function(){
            console.log("handle unauthorized");
        }

        return {
            'request': function (config) {
                console.log(config.url);
                self.blockUI();
                //config.headers['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT'; // Disables IE AJAX request caching
                //config.headers['Cache-Control'] = 'no-cache'; // Disables caching
                //config.headers['Pragma'] = 'no-cache'; // Disables caching
                config.headers['X-Requested-With'] = 'XMLHttpRequest';
                config.xsrfCookieName = 'csrftoken';
                config.xsrfHeaderName = 'X-CSRFToken';
                return config;
            },
            'requestError': function (rejection) {
                self.handleHttpError();
                self.unblockUI();
                return $q.reject(rejection);
            },
            'response': function (response) {
                if (self.isUnauthorized(response)) {
                    response = false;
                }
                else if (self.isForbidden(response)) {
                    response = false;
                    self.handleForbidden();
                }
                else if (self.responseHasError(response)) {
                    response = false;
                    self.handleHttpError();
                }
                //$timeout(setWindowElementHeights, 100);
                self.unblockUI();
                return response;
            },
            'responseError': function (rejection) {
                if (rejection.status == 401) {
                    $injector.get("sessionService").redirectToLoginPage();
                }
                else if (rejection.status == 403) {
                    self.handleForbidden();
                }
                else {
                    self.handleHttpError();
                }
                self.unblockUI();
                return $q.reject(rejection);
            }
        }
    }]);