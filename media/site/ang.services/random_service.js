angular.module('MPlayer').factory('randomNumberService', [
function ($rootScope, $http, $q, musicService) {
    var service = { };
    service.getRandomNumber = function (st,end) {
        return Math.floor(Math.random() * end) + st
    };
    return service;
}]);