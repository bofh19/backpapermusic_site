angular.module('MPlayer').factory('playlistService',
    ['$rootScope', '$http', '$q', 'musicService',
        function ($rootScope, $http, $q, musicService) {
        var service = {
            playlists: [],
            observers: [],
            q: []
        };

        service.onPlaylistUpdated = function (observer) {
            service.observers.push(observer);
        }

        place_song = function(i,song_id){
            musicService.getSong(song_id)
            .then(function(song){
                if(song != undefined){
                    service.playlists[i].songs.push(song);
                }
            });
        }

        //these methods have to be called using api
        // so using promise to fake it for now 
        service.getPlaylists = function () {
            if(service.playlists.length <= 0){
                return $http.get("/user/playlist/all")
                .then(function(response){
                    if(response.data.status){
                        service.playlists = [];
                        service.playlists = response.data.result;
                        for (var i = 0; i < service.playlists.length; i++) {
                            var pl = service.playlists[i];
                            //pl.songs.push(4629);
                            //pl.songs.push(-1362755383);
                            //pl.songs.push(-1749848308)
                            song_ids = [];
                            angular.copy(pl.songs, song_ids);
                            pl.songs = [];
                            console.log(song_ids);
                            for(var j=0; j < song_ids.length; j++){
                                place_song(i, song_ids[j]);
                            }
                        }
                    };
                    return service.playlists;
                });
            }else{
                return $q.when(service.playlists);
            }
        };

        service.addSong = function (playlistId, song, position) {
            return $http.post("/user/playlist/add/",{'playlist_id': playlistId, 'song_id': song.id})
                .then(function(response){
                    for(i = 0 ; i < service.playlists.length; i++){
                        var pl = service.playlists[i];
                        if(pl.id == playlistId){
                            var s = {};
                            angular.copy(song, s);
                            pl.songs.push(s);
                            angular.forEach(service.observers, function(observer){
                                observer(pl);
                            });
                        };
                    };
                });
        };

        service.createPlaylist = function (playlistName) {
            return $http.post("/user/playlist/create/", { name : playlistName }).then(function(response){
                if(response.data.status){
                    service.playlists.push(response.data.playlist);
                        angular.forEach(service.observers, function (observer) {
                        observer(response.data.playlist);
                    });
                };
                return response.data.playlist;
            });
        };

        service.getPlaylist = function (playlist_id) {
            return service.getPlaylists()
                .then(function(data){
                    for(i=0;i < data.length; i++){
                        pl = data[i];
                        if(pl.id == playlist_id){
                            return pl;
                        }
                    }
                    return undefined;
                });
        };

        service.qSong = function (song) {
            return $http.post("/user/q/add/",{'song_id': song.id})
                .then(function(response){
                    if(response.data){
                        service.q.push(song);
                    }
                });
        };

        service.getQ = function(){
            if(service.q.length <= 0){
                return $http.get("/q/get/")
                    .then(function(response){
                        debugger;
                        service.q = response.data.result;
                    });
            }else{
                $q.when(service.q);
            }
        }

        return service;
    }]);