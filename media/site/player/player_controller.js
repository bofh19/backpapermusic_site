angular.module("MPlayer")
    .controller("PlayerController", ['$rootScope', 'musicService', 'audioPlayerService',
        function ($rootScope, musicService, audioPlayerService) {
            var self = this;
            self.albums_internal = [];
            self.albums = [];
            self.start_letters = [];
            self.click_letter = "";
            self.init = function () {
                musicService.getAlbums().then(function (data) {
                    self.albums_internal = data;
                    angular.forEach(self.albums_internal, function(album){
                        self.start_letters.push(album.album[0]);
                    });
                    self.start_letters = Array.from(new Set(self.start_letters));
                    self.albums = self.albums_internal;
                });
            };


            self.letter_clicked = function(letter){
                self.albums = [];
                angular.forEach(self.albums_internal, function(album){
                    if(album.album[0] == letter){
                        self.albums.push(album);
                    }
                });
            };

            self.albumClicked = function (album) {
                if(album.showSongs){
                    album.showSongs = false;
                    return;
                };
                angular.forEach(self.albums, function(a){
                    a.showSongs = false;
                });
                album.showSongs = true;
                if (album.songs == undefined || album.songs.length <= 0) {
                    musicService.getAlbumSongs(album.album).then(function (data) {;
                        album.songs = data;
                    });
                }
                
            };

            self.playSong = function(song, action){
                audioPlayerService.playSong(song);
            };
        }]);