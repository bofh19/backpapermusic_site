window.utils = {};

window.utils.getOffset = function (el) {
    el = el.getBoundingClientRect();
    return {
        left: el.left + window.scrollX,
        top: el.top + window.scrollY
    }
};

window.utils.documentHeight = function(){
    return Math.max($(document).height(), $(window).height());
}

window.utils.documentWidth = function(){
    return Math.max($(document).width(), $(window).width())
}

window.utils.scrollPos = function(){
    var doc = document.documentElement;
    var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
    var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
    return {
        top: top,
        left: left
    };
}